#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import sys
import time

import numpy as np
import matplotlib.pyplot as plt

import FtrFile


def euclidean_distance(vec1, vec2):

    vec1 = np.multiply(vec1, [1.0, 0.9, 0.5, 0.4, 0.3, 0.2, 0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.01])
    vec2 = np.multiply(vec2, [1.0, 0.9, 0.5, 0.4, 0.3, 0.2, 0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.01])

    # return np.sum(np.abs(vec1 - vec2))
    # return np.sum(np.power(vec1 - vec2, 2))

    return np.sqrt(np.sum(np.power(vec1 - vec2, 2)))


#########################################
# State, Graph, etc...
#########################################

class State:
    def __init__(self, ftr, idx):  # idx is for debug purposes
        self.ftr = ftr
        self.word = None
        self.isFinal = False
        self.nextStates = []
        self.idx = idx


class Link:
    def __init__(self, to_state, intermediate_states=[]):

        self.to_state = to_state
        self.intermediate_states = intermediate_states


def print_state(state):
    nextStatesIdxs = [s.to_state.idx for s in state.nextStates]
    inter_states = [s.intermediate_states for s in state.nextStates]
    print("State: idx={} word={} isFinal={} nextStatesIdxs={} ftr={} inter_states {}".format(
        state.idx, state.word, state.isFinal, nextStatesIdxs, state.ftr, inter_states))


def load_graph(etalons_features, step=float('inf')):
    startState = State(None, 0)
    max_samples = 0

    graph = [startState, ]
    stateIdx = 1

    for word, features in etalons_features.items():
        prevState = startState
        max_samples = max(max_samples, len(features))

        step_idx_id = 0
        step_state = None

        for frame in range(len(features)):

            ''' Create new state '''
            state = State(features[frame], stateIdx)

            ''' Add loop back link to the state '''
            state.nextStates.append(Link(state))  # add loop

            ''' Add link from prev state to new'''
            prevState.nextStates.append(Link(state))

            ''' Add link to not adjacent state '''
            if step_idx_id >= step:
                step_state.nextStates.append(Link(state, list(range(step_state.idx + 1, state.idx))))

                step_state = graph[step_state.idx + 1]

            ''' Set new state as prev '''
            prevState = state

            ''' Set initial state for step_state '''
            step_state = step_state or state

            ''' Add state to graph '''
            graph.append(state)
            stateIdx += 1
            step_idx_id += 1

        if state:
            state.word = word
            state.isFinal = True

    return graph, max_samples


def check_graph(graph):
    assert len(graph) > 0, "graph is empty."
    assert graph[0].ftr is None \
        and graph[0].word is None \
        and not graph[0].isFinal, "broken start state in graph."
    idx = 0
    for state in graph:
        assert state.idx == idx
        idx += 1
        assert (state.isFinal and state.word is not None) \
            or (not state.isFinal and state.word is None)


def print_graph(graph):
    print("*** DEBUG. GRAPH ***")
    np.set_printoptions(formatter={'float': '{: 0.1f}'.format})
    for state in graph:
        print_state(state)
    print("*** END DEBUG. GRAPH ***")


#########################################
# Token
#########################################

class Token:
    def __init__(self, state, dist=0.0, sentence=""):
        self.state = state
        self.dist = dist
        self.sentence = sentence


def print_token(token):
    print("Token on state #{} dist={} sentence={} word={}".format(token.state.idx,
                                                          token.dist,
                                                          token.sentence,
                                                          token.state.word))


def print_tokens(tokens):
    print("*** DEBUG. TOKENS LIST ***")
    for _, token in tokens.items():
        print_token(token)
    print("*** END DEBUG. TOKENS LIST ***")


def print_result_tokens_to_file(filename, final_tokens, winner_token, file_with_results):
    """ Print to file result all active tokens and the winner token """

    res_out_file = open(file_with_results, 'a')
    res_out_file.write('#' * 33 + '\n')
    res_out_file.write('#' * 10 + '  ' + filename + '  ' + '#' * 10 + '\n')
    res_out_file.write('#' * 33 + '\n')

    ''' Write all active tokens to file '''
    for _token in final_tokens:
        res_out_file.write('State: ' + str(_token.state.idx) +
                           ' dist: ' + str(_token.dist) +
                           ' word: ' + _token.state.word + '\n')

    ''' Write the Winner token '''
    res_out_file.write('Winner token :\n')
    res_out_file.write('State: ' + str(winner_token.state.idx) +
                       ' dist: ' + str(winner_token.dist) +
                       ' word: ' + winner_token.state.word + '\n')


def print_result_for_kaldi_wer(filename, winner_token, file_with_results):

    res_out_file = open(file_with_results, 'a')

    res_out_file.write(filename + ' ' + winner_token.state.word.split('_')[0] + '\n')

#########################################
# Decoder
#########################################
def recognize_step(next_tokens, ftr, graph, thr_common=float('inf')):
    """ One step for token passing algorithm """

    ''' Set nextTokens as active tokens. Clear nextTokens  '''
    active_tokens = next_tokens
    next_tokens = dict()

    ''' Init min_distance to realize thm_common '''
    min_dist = float('inf')

    ''' Create cache to calculate distance between states ftr and a input ftr '''
    distance_cach = dict()

    for _, _token in active_tokens.items():

        ''' Generate all possible new tokens '''
        for _next_state in _token.state.nextStates:

            ''' Calc distance between state and new ftr '''
            if not distance_cach.get(_next_state.to_state, None):
                distance_cach[_next_state.to_state] = euclidean_distance(ftr, _next_state.to_state.ftr)

            new_dist = _token.dist + distance_cach[_next_state.to_state] # euclidean_distance(ftr, _next_state.to_state.ftr)

            ''' If new token jump to not adjacent state need to add distance between all skipped stated '''
            for _skipped_state_idx in _next_state.intermediate_states:

                if not distance_cach.get(graph[_skipped_state_idx], None):
                    distance_cach[graph[_skipped_state_idx]] = euclidean_distance(ftr, graph[_skipped_state_idx].ftr)

                new_dist += distance_cach[graph[_skipped_state_idx]] # euclidean_distance(ftr, graph[_skipped_state_idx].ftr)

            ''' Save min distance for thm_common '''
            min_dist = min(min_dist, new_dist)

            ''' Create a new token '''
            new_token = Token(_next_state.to_state, new_dist)

            ''' Create new token '''
            token_in_state = next_tokens.get(new_token.state.idx, None)

            ''' Don't add new token If a token in state has less dist '''
            if token_in_state and token_in_state.dist < new_token.dist:
                continue

            ''' Add/replace new token for the next step '''
            next_tokens[new_token.state.idx] = new_token

    ''' beam pruning '''
    states_to_del = []

    for _state_id in next_tokens:
        if next_tokens[_state_id].dist > min_dist + thr_common:
            states_to_del.append(_state_id)

    for _state in states_to_del:
        del next_tokens[_state]

    # print('len after beam : {}'.format(len(next_tokens)))

    return next_tokens


def recognize(data_to_recognize, graph, max_samples, file_with_results, file_for_wer, thr_common=float('inf')):
    filename = data_to_recognize[0]
    features = data_to_recognize[1]

    start_state = graph[0]
    # nextTokens = [Token(startState), ]
    next_tokens = {start_state.idx: Token(start_state)}

    last_idx = 0
    for _idx in range(features.nSamples):
        ''' Get next feature vector '''
        _ftr = features.readvec()

        # print('#' * 30)
        # print_tokens(activeTokens)
        # print('Nsampl: ', features.nSamples, ' cur: ', last_idx, ' len: ', len(next_tokens))

        next_tokens = recognize_step(next_tokens, _ftr, graph, thr_common)
        last_idx += 1

        # print('idx = {}/{} len_tokens = {}'.format(_idx, features.nSamples, len(next_tokens)))
        # print(len(next_tokens))

    # print('len graph : {}'.format(len(graph)))

    ''' Get result '''
    final_tokens = []
    for _, _token in next_tokens.items():
        if _token.state.isFinal:
            final_tokens.append(_token)

    distance = [_token.dist for _token in final_tokens]

    print('\n' + '#' * 30)
    print("Recognizing file '{}', samples={}".format(filename,
                                                     features.nSamples))

    if len(final_tokens) == 0:
        print('Not final tokens found')
    else:
        print('---------- Winner ----------')
        winner_token = final_tokens[distance.index(min(distance))]
        print_token(winner_token)

        print_result_tokens_to_file(filename, final_tokens, winner_token, file_with_results)
        print_result_for_kaldi_wer(filename, winner_token, file_for_wer)

        return filename.split('_')[0].find(winner_token.state.word.split('_')[0]) != -1

    return False

#########################################
# Help functions
#########################################
def find_max_etalon_size(etalon_signals):

    max_len = 0

    for _name, _etalon_sig in etalon_signals.items():
        max_len = max(max_len, len(_etalon_sig))

    return max_len


def get_avarage_feature(ftr_vector):

    res_ftr = ftr_vector[-5]

    for i in range(4):
        res_ftr += ftr_vector[-5 + 1 + i]

    res_ftr /= 5.0

    return res_ftr


def normalize_etalons(etalons, etalons_length):

    for _name in etalons:

        while len(etalons[_name]) < etalons_length:
            etalons[_name].append(get_avarage_feature(etalons[_name]))


def read_features(ark_filename):

    features = dict()

    for _filename, _ftr in FtrFile.FtrDirectoryReader(ark_filename):

        feature_data = []

        for i in range(_ftr.nSamples):
            feature_data.append(_ftr.readvec())

        features[_filename] = feature_data

    return features

#########################################
# Main
#########################################

from multiprocessing import Pool
from functools import partial

threads_count = 8

BASE_PATH = '/home/alex/projects/pycharm/ASRlesson040'


def base_recognition(etalons, records, result_file_names):
    """ """

    etalons_features = read_features(etalons)

    ''' Normalize features length '''
    max_etalon_length = find_max_etalon_size(etalons_features)
    normalize_etalons(etalons_features, max_etalon_length)

    graph, max_samples = load_graph(etalons_features, 30)
    check_graph(graph)
    # print_graph(graph)

    ''' Remove old result files '''
    result_filename = result_file_names['result_filename']
    if os.path.exists(result_filename):
        os.remove(result_filename)
    file_for_wer = result_file_names['file_for_wer']
    if os.path.exists(file_for_wer):
        os.remove(file_for_wer)
    file_wer_train = result_file_names['file_wer_train']
    if os.path.exists(file_wer_train):
        os.remove(file_wer_train)

    ''' Set thread count for parallel calculation '''
    p = Pool(threads_count)
    sys.setrecursionlimit(10000)

    ''' Create train file for compute-wer '''
    train_file = open(file_wer_train, 'a')

    ''' Create initial data in vectors to run recognizers in parallel '''
    to_recognize_data = []
    for filename, features in FtrFile.FtrDirectoryReader(records):
        to_recognize_data.append((filename, features))
        train_file.write(filename + ' ' + filename.split('_')[0] + '\n')

        # if filename.find('da_15') != -1:
        #     print('Start recognition')
        #     start_time = time.time()
        #     res = recognize([filename, features], graph, max_samples, result_filename, file_for_wer, 50)
        #     print(res)
        #     print('Stop recognition time: {:.5}s'.format(time.time() - start_time))
        #
        #     break

    wer = []
    calculation_time = []
    # thr_common = (100, 200, 300, 400, 500, 600)
    thr_common = [float('inf')] #range(1500, 2100, 100)

    for _thr in thr_common:

        ''' Run recognition '''
        print('Start recognition')
        start_time = time.time()
        res = p.map(partial(recognize, graph=graph, max_samples=max_samples,
                            file_with_results=result_filename, file_for_wer=file_for_wer,
                            thr_common=_thr),
                    to_recognize_data)

        err = 0
        for _recog_res in res:
            if not _recog_res:
                err += 1
        wer.append(err / len(res) * 100)
        calculation_time.append(time.time() - start_time)
        print('Stop recognition time: {:.5}s'.format(calculation_time[-1]))

        print('Th: ', _thr)
        print('WER: ', wer)
        print('Calc time: ', calculation_time)

    # wer = [100.0, 90.0]
    # calculation_time = [31.439409494400024, 48.398776054382324]

    # plt.plot(calculation_time, wer)
    # for x, y, _th in zip(calculation_time, wer, thr_common):
    #     plt.text(x, y, _th)
    # plt.show()

def da_net_recognize():

    etalons = 'ark,t:' + BASE_PATH + '/data/da_net_etalons.txtftr'
    records = 'ark,t:' + BASE_PATH + '/data/da_net_test.txtftr'

    base_recognition(etalons, records,
                     {'result_filename': 'result.txt',
                      'file_for_wer': 'wer_results.txt',
                      'file_wer_train': 'wer_train.txt'}
                     )


def digits_recognition():
    etalons = 'ark,t:' + BASE_PATH + '/data/digits_etalons.txtftr'
    records = 'ark,t:' + BASE_PATH + '/data/digits_test.txtftr'

    base_recognition(etalons, records,
                     {'result_filename': 'digit_result.txt',
                      'file_for_wer': 'digit_wer_results.txt',
                      'file_wer_train': 'digit_wer_train.txt'}
                     )


if __name__ == "__main__":

    da_net_recognize()

    # digits_recognition()


# /home/alex/projects/ASR/kaldi/src/bin/compute-wer --text --mode=present ark:wer_train.txt ark:wer_results.txt
# /home/alex/projects/ASR/kaldi/src/bin/compute-wer --text --mode=present ark:digit_wer_train.txt ark:digit_wer_results.txt
